'use strict';

// Use local.env.js for environment variables that grunt will set when the server starts locally.
// Use for your api keys, secrets, etc. This file should not be tracked by git.
//
// You will need to set these on the server you deploy to.

module.exports = {
  METERS: {
    fibaroId : { //change to fibaro id
      energonId: energonId, //change to energon id
      name: ""
    }
  },
  FIBARO_USER: 'api', //user which can see fibaro devices
  FIBARO_SECRET: "", //user pwd in fibaro HC2
  FIBARO_URL: "http://xyz/api", //fibaro HC2 url

  ENERGON_TOKEN: '', //energon apitoken
  ENERGON_URL: 'https://energon.azurewebsites.net/api',

  // Control debug level for modules using visionmedia/debug
  DEBUG: ''
};
