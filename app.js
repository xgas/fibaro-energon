var env = require('./config/local.env');
var request = require('request');
var url = require('url');
var moment = require('moment');

var app = {};

app.getData = function(callback) {
    request.get(env.FIBARO_URL + "/devices",
        {
            qs: {
                type: "com.fibaro.temperatureSensor"
            },
            'auth': {
                'user': env.FIBARO_USER,
                'pass': env.FIBARO_SECRET,
                'sendImmediately': false
            }
        },
        function (error, response, body) {
            console.log(response.statusCode);
            if (!error && response.statusCode == 200) {
                callback(JSON.parse(body));
            }
        }
    );
};

var parseTemperature = function(data) {
    var sensors = [];
    for(var i in data) {
        var sensor = {};
        sensor.id = data[i].id;
        sensor.name = data[i].name;
        sensor.temperature = data[i].properties.value;
        console.log(sensor);
        sensors.push(sensor);
    }
    return sensors;
};

var sendToEnergon = function(data) {
    var data = parseTemperature(data);
    var now = moment.utc().toISOString();
    for(var i in data) {
        var sensor = data[i];
        var energonMeter = env.METERS[sensor.id];
        if (energonMeter) {
            var postData = {
                measures: [
                    {
                        date: now,
                        value: sensor.temperature
                    }
                ]
            };
            console.log(postData);
            request.post(env.ENERGON_URL + '/meters/' + energonMeter.energonId + "/measures",
                {
                    qs: {
                        api_token: env.ENERGON_TOKEN
                    },
                    json: postData
                },
                function(error, response){
                    console.log(error);
                    console.log(response.statusCode);
                }
            )
        }
    }
};

module.exports = app;
app.getData(sendToEnergon);
